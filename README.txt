
============================================================
Raid On Code Pirate (ROCOP) - A Plagiarism Detection System
============================================================

One of the most noticable issue with the substantially growing size of 
internet is plagiarism or digital copying. The administrator can create a 
web base containing the pages of the websites mined from the internet and 
insert the fingerprints of those webpages into the database. The end users 
can upload the document to check for the possible plagiarism from those websites.

All code is under MIT license, see LICENSE for details.

Source: http://svn.collaborate.d2labs.org/svn/rocop/

=============
Requirements
=============

* Django>=1.2.3
* MySQLdb>=2.0
* Python==2.6.x
* Chilkat==2.0.0
* Django-celery==2.1.4
* MySQL>=5.1.419
* Operating System>=Ubuntu 9.10 (32 bit)

==============
Configuration
==============

 -------------------------
| Copying the source code |
 -------------------------

1. Copy the directory `rocop` to your convenient place.
2. Change the permission of the directory recursively to 'rw'. In the shell prompt;
	$ sudo chmod 765 rocop -R

 ---------------------
| Setting up Database |
 ---------------------

1. Create a database named `rocop`. In the shell prompt;
	$ /usr/bin/mysql -u <username> -p 
	mysql> CREATE DATABASE rocop;

2. Import the dump `rocop_django_sql_dump.sql` into the database `rocop`. In the shell prompt;
	$ mysql -u <username> -p rocop < rocop_django_sql_dump.sql 

3. Create a database named `rocop_fp`. In the shell prompt;
	mysql> CREATE DATABSE rocop; 

4. Create a table named `fingerprint` with two columns `fp` for fingerprint and `documents` for document id.
   Index the `fingerprint` table with `fp` column. In the shell prompt;
	mysql> CREATE TABLE `fingerprint` (fp BIGINT, documents TEXT, INDEX (fp));

5. Change the user & passwd inside connect_to_db() method in DBHandler.py inside the dbmanager directory.

 -------------------------------
| Running the production server |
 -------------------------------

1. Move inside the directory `rocop_web` from the command prompt. If `rocop` directory is in home directory
	$ cd /home/rocop/rocop_web
2. Run the web server manually 
	$ python manage.py runserver [args]
	If you want to run the server in localhost, [args] is empty. Otherwise if you want other machines in LAN to be able 
	to access it then use 0.0.0.0:8080 as [args]

 ----------------------
| Accessing the system |
 ----------------------
1. The web front end of the system can be accessed from any browser using http://x:8080 where x=localhost if [args] is empty else
	x=0.0.0.0, in the address bar

 -------------------------------------
| Using Admin panel for back end tasks|
 -------------------------------------

1. Move to the login page and login as an administrator using username=admin, password=a
2. You must specify the website name and the number of pages to crawl in the input box before starting crawling
3. The database can be updated afterward with the fingerprints of the crawled pages afterwards with just a single click

 --------------------------------------------
| Looking the back-end asynchronous task log |
 --------------------------------------------

1. Move inside the directory `rocop_web` from the command prompt. If `rocop` directory is in home directory
	$ cd /home/rocop/rocop_web

2. Run the following in the command prompt;
	$ python manage.py celeryd -l info

 -------------------------------
| Using the system for checking |
 -------------------------------
1. Login to the system as a simple end-user
2. Upload the file under suspection 

====
1.0
====

Initial release
