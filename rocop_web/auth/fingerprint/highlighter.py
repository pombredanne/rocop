#!/usr/bin/python 
#
# fingerprint/highlighter.py - highlights the matching substring in the original
#                              file according to the position of matching k-gram
#                              Algorithm used here is not efficient since it 
#                              omits the substring match on the starting of the
#                              file if matching k-gram is the one from the last
#
# Copyright (c) 2010, ROCOP
#

#
# Imports
#

from settings import MEDIA_ROOT

#
#
#

#
# The `highlight_original_text` method
#

def highlight_original_text(docName, matchPos, kLength):
    '''
    Highlights the substring match in the original text
    Each character is tagged, white spaces are removed, each matching k-gram 
    position is mapped to the original text position later
    '''
    #f = open("/home/kailash/workspace/rocop/temp/%s"%docName,'r')
    f = open("%s/%s"%(MEDIA_ROOT,docName),'r')
    k = f.read()
    l = "".join([c for c in k if c not in ('\r')])
    l = l.replace('\n', ' ')
    matchPos.append(-1)
    
    '''
    1) Tag each character in the document with it's position
    2) Remove whitespace characters from the tag list
    '''
    d = []
    for i in xrange(0, len(l)):
        d.append([l[i], i])
    popList = []
    new = d
    for i in xrange(0, len(new)):
        if new[i][0] == ' ':
            popList.append(i)
    for i in xrange(0, len(popList)):
        new.pop(popList[i]-i)

    '''
    Find the consecutive fingerprint match position from the kgram position
    and make a (start, end) tuple
    '''
    consecutive = 0
    startEndList = [] 
    for i in xrange(1, len(matchPos)):
        front = matchPos[i]
        back = matchPos[i-1]
        if front == -1:
            if consecutive == 1:
                #startEndList.append((start, len(new)-1))
                startEndList.append((start, back+kLength))    
            else:
                #startEndList.append((back, len(new)-1))
                startEndList.append((back, back+kLength))
        if back+kLength >= front:
            if consecutive == 0:
                start = back
            consecutive = 1
        else:
            if consecutive == 0:
                startEndList.append((back, back+kLength))
            else:
                startEndList.append((start, back+kLength))
            consecutive = 0
    #print len(new)        
    #print matchPos
    print startEndList
    
    '''
    Get the actual string from the document using the start and end pos which 
    is obtained by mapping the start and end pos to the whitespace removed list
    '''
    temp = new[startEndList[0][0]][1]
    prevEnd = 0
    s = l[:temp]
    for item in startEndList:
        start = new[item[0]][1]
        end = new[item[1]][1]
        if prevEnd:
            s = s+l[prevEnd:start]
        s = s+'<p id="match">'+l[start:end]+'</p>'
        prevEnd = end
    if prevEnd <= len(l):
        s = s+l[prevEnd:]  
    return s
    
