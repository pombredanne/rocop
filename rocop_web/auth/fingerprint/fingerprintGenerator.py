#!/usr/bin/python 
#
# fingerprint/fingerprintGenerator.py - generates the fingerprints of a string
#
# Copyright (c) 2010, ROCOP
#

#
# Imports
#

import gc
from circularBuffer import *

#
#
#

#
# The `FingerprintGenerator` class
#

class FingerprintGenerator:
    '''
    An action class whose instance is used for creating fingeprints of string
    '''
    def __init__(self, inputString, kLength=50, base=101, divisor=2**32, windowSize=100):
        self.docString = inputString
        self.base = base
        self.divisor = divisor
        self.kgrams = []
        self.hashValues = []
        self.kLength = kLength
        self.windowSize = windowSize
        self.hashWindows = []
        self.fingerprints = []
        self.index = 0
        self.prev_hash_pos_local = 0
        self.prev_hash_pos_global = 0
        self.circList = []

    def generate_kgrams(self):
        '''
        Generates the kgrams from the given string
        '''
        totalChars = len(self.docString)
        frontCount = 1
        for i in xrange(0, totalChars):
            if (i+self.kLength)>len(self.docString):
                kgram = self.docString[i:]+self.docString[:frontCount]
                frontCount += 1
            else:
                kgram = self.docString[i:i+self.kLength]
            self.kgrams.append(kgram)
        
    def generate_rollingHash(self):
        '''
        Generates the hash values for each kgrams using Karp-Rabin rolling hash function
        '''
        firstKgram = self.kgrams[0]
        prevHashValue = 0
        for i in xrange(0, self.kLength):
            prevHashValue += ord(firstKgram[i])*(self.base**(self.kLength-1-i))
        # Improvement on Rolling Hash | Each character potentially affect all of the hash's bit
        prevHashValue *= self.base
        self.hashValues.append(prevHashValue%self.divisor)
        # using this line just for testing 
        prevHashValue = prevHashValue%self.divisor
        for i in xrange(0, len(self.kgrams)-1):
            #newHashValue = (prevHashValue-ord(self.docString[i])*self.base**(self.kLength-1))*self.base+ord(self.kgrams[(i+self.kLength)%self.kgrams.__len__()][0])
            # Improvement on Rolling Hash | Each character potentially affect all of the hash's bit
            # Python's implied line continuation used here
            newHashValue = ((prevHashValue-ord(self.docString[i])*self.base**(self.kLength))+\
                            ord(self.kgrams[(i+self.kLength)%self.kgrams.__len__()][0]))*self.base
            prevHashValue = newHashValue
            # using this line for testing
            prevHashValue = prevHashValue%self.divisor
            self.hashValues.append(newHashValue%self.divisor)
        #print 'hash=',len(self.hashValues)
        self.circList = CircularList(self.hashValues)
            
    def next_hash(self):
        '''
        Provides the next hash value from a circular hash list for winnowing
        '''
        #global i
        #circList = CircularList(self.hashValues)    
        nextHash = self.circList[self.index]        
        self.index += 1
        return nextHash
    
    def record_global_pos(self, min_hash, min_hash_pos):
        '''
        Records the global position of a fingerprint from its local position within the window				
        '''
        #global prev_hash_pos_global
        #global prev_hash_pos_local
        if self.prev_hash_pos_local<min_hash_pos:
            temp_pos = min_hash_pos-self.prev_hash_pos_local
        else:
            modified_new_pos = self.windowSize+min_hash_pos
            temp_pos = modified_new_pos-self.prev_hash_pos_local
        new_hash_pos_global = temp_pos+self.prev_hash_pos_global
        self.fingerprints.append([min_hash, new_hash_pos_global])
        self.prev_hash_pos_global = new_hash_pos_global
        self.prev_hash_pos_local = min_hash_pos
        
    def winnow(self):
        self.circList = CircularList(self.hashValues)
        '''
        Carries winnowing operation to produce fingerprints fromt the hash values
        '''
        h = [] 
        for i in xrange(0, self.windowSize):
            h.append(self.next_hash())
        r = self.windowSize-1 			      	# window right end
        min_hash_pos = self.windowSize-1		# index of minimum hash
        for i in xrange(0, len(self.hashValues)-self.windowSize+1):
            if min_hash_pos==r:
                # The previous minimum is no longer in this window 
                # Scan h leftward starting from r for the rightmost minimal hash
                min_hash = min(h)
                min_hash_pos = h.index(min_hash)
                self.record_global_pos(min_hash, min_hash_pos)
            else:
                # Otherwise the previous minimum is still in this window 
                # Compare against the new value and update min if necessary
                if h[r] <= min_hash:
                    min_hash_pos = r
                    min_hash = h[r]
                    self.record_global_pos(min_hash, min_hash_pos)
            r = (r+1)%self.windowSize   # shift the window by one
            h[r] = self.next_hash()
        #print self.fingerprints
        #self.fingerprints = []
        #i = 0
        #for v in self.hashValues:
        #    temp = []
        #    temp.append(v)
        #    temp.append(i)
        #    self.fingerprints.append(temp)
        #    i += 1
            
    def generate_fingerprints(self):
        '''
        Method that can call all other methods within the class
        '''
        self.generate_kgrams()
        self.generate_rollingHash()
        self.winnow()

    '''
    def compare_fingerprints(self, fingerprintsOnDB, kgramsOnDB):
        # Compare according to first element of nested list and store the [FP, hash] for both documents
        sameFP = []
        for i in xrange(0, len(self.fingerprints)):
            checkTemp = self.fingerprints[i][0]
            for j in xrange(0, len(fingerprintsOnDB)):
                dbTemp = fingerprintsOnDB[j][0]
                if checkTemp == dbTemp:
                    sameFP.append(fingerprintsOnDB[j])
        print '-'*59
        for i in xrange(0, len(sameFP)):
            pos = sameFP[i][1]
            print kgramsOnDB[pos]
            print '-'*59
'''
#newFP = FingerprintGenerator("Thissectionexplainsthebasicconceptoffunctionalprogramming", 30, 13, 2**16, 4)
#newFP.winnow()
#startTime = time.time()
'''
checkFp = FingerprintGenerator("This section explains the basic concept of functional programming", 20, 13, 2**8, 4)
checkFp.generate_fingerprints()
#checkFp.compare_fingerprints(newFP.fingerprints, newFP.kgrams)
FP = open("/home/kailash/workspace/rocop/repo/stackoverflow.com/33.txt", 'r')
allStr = FP.read()
newlineRemoved = "".join([c for c in allStr if c not in('\n', '\r' ,' ')])
newlineRemoved = newlineRemoved.lower()

fp = FingerprintGenerator(newlineRemoved)
fp.generate_fingerprints()
#fp.compare_fingerprints(newFP.fingerprints, newFP.kgrams)
print fp.fingerprints
FP.close()
s1 = 'klength=50, base=41, divisor=2**32'
windowSizeList = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
s2 = 'HASH_COUNT			FP_COUNT			WINDOW_SIZE'
print s1
print s2
f = open("/home/kailash/Major Project/FYP-REPORT/WindowSizeVsFPCount.txt", "w")
f.write(s1+"\n"+s2+"\n")
f.close()
base_path = "/home/kailash/workspace/rocop/temp"
for wSize in windowSizeList:
	fp = open("%s/%s"%(base_path, "1.txt"),"r")
	rawStr = fp.read()
	refinedStr = ''.join([c for c in rawStr if c not in('\n', '\r', ' ')])
	refinedStr = refinedStr.lower()
	#if refinedStr=="": Yet to be implemented			
	thisDoc = FingerprintGenerator(refinedStr, 50, 41, 2**32, wSize)
	thisDoc.generate_fingerprints()
	s3 = "%d"%len(thisDoc.hashValues)+'\t\t\t\t%d'%len(thisDoc.fingerprints)+'\t\t\t\t\t%d'%thisDoc.windowSize+"\n"
	f = open("/home/kailash/Major Project/FYP-REPORT/WindowSizeVsFPCount.txt", "a")
	f.write(s3)	
	f.close()
	fp.close()
	print s3
		
base_path = "/home/kailash/Desktop/"
fp = open("%s/%s"%(base_path, "1.txt"),"r")
rawStr = fp.read()
refinedStr = ''.join([c for c in rawStr if c not in('\n', '\r', ' ')])
refinedStr = refinedStr.lower()
print 'Total Chars=',len(refinedStr)			
thisDoc = FingerprintGenerator(refinedStr)
cProfile.run('thisDoc.generate_fingerprints()')
'''
