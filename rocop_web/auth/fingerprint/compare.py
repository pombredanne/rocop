#!/usr/bin/python 
#
# fingerprint/compare.py - performs the comparison of input documents' fingerprints with the DB
#                          and returns the matched docs 
#
# Copyright (c) 2010, ROCOP
#

#
# Imports
#

from __future__ import division
import cProfile
from fingerprintGenerator import * 
from auth.dbmanager.DBHandler import * 
from settings import *
from collections import defaultdict
from operator import itemgetter
from highlighter import *

#
#
#

#
# The `Document` class
#

class Document:
    '''
    Document objects represent input documents and their associated attributes
    '''
    def __init__(self, docName):
        self.docName = docName
        self.kgrams = []
        self.fingerprints = []
        self.allMatchDict = {}
        self.allMatchList = []
        self.matchDict = []
        self.docToFPDict = {}
        self.docToPercentMatch = []
        self.docToKgramMatch = {}
        self.matchCount = 0
        self.lessContentFlag = 0
        self.allInfo = []
        self.allKgramPos = []

    def sort_match_doc_freq(self):
        '''
        Rank-orders by size(number of fingerprints) the matches between documents
        '''
        localmatchDict = defaultdict(int)
        for k in self.allMatchDict.values():
            for u in k:
                self.allMatchList.append(u)
        
        for doc in self.allMatchList:
            localmatchDict[doc]+=1
        self.matchDict = localmatchDict.items()
        self.matchDict = sorted(self.matchDict, key=itemgetter(1), reverse=True)
        for item in self.matchDict:
            docName = item[0]
            totalFPMatch = item[1]
            docIDList = []
            temp = docName.split("-")
            #f = open("/home/kailash/workspace/rocop/repo/%s/mapper.log"%temp[0], "r")
            f = open("%s/%s/mapper.log"%(REPO_PATH,temp[0]), "r")
            for line in f.read().split('\n'):
                if line.startswith(temp[1]+";"):
                    docIDList = line.split(";")
                    break
            self.docToPercentMatch.append((docIDList[1], (totalFPMatch/len(self.fingerprints))*100))
        self.allInfo.append(self.docToPercentMatch)

        for i in xrange(0,len(self.matchDict)):
            doc_name = self.matchDict[i][0]
            temp = []
            for k in self.allMatchDict.keys():
                l = self.allMatchDict[k]
                if doc_name in l:
                    temp.append(k)
            self.docToFPDict.update({doc_name:temp})
        self.get_kgram_from_fp()

    def check(self):
        '''
        Finds the fingerprint to list of documents mapping
        '''
        for fprint in self.fingerprints:
            allMatchListForSingleFP = []
            dbHandler = DBHandler()
            dbHandler.connect_to_db()
            singleMatchList = dbHandler.check_fp(fprint[0])
            # Use a dictionary mapping fingerprint to document frequency 
            # Then calculate the overall document frequency for all fp 
            # Use that to calculate the overall plagiarism percentage
            if singleMatchList != 0:
                self.matchCount += 1
                for doc in singleMatchList:
                    allMatchListForSingleFP.append(doc)
                self.allMatchDict.update({fprint[0]:allMatchListForSingleFP})
        #print self.allMatchDict
        #print '-'*44

    def read_file(self):
        '''
        Reads the input document and generates its fingerprints
        '''
        #base_path = "/home/kailash/workspace/rocop/temp"
        base_path = MEDIA_ROOT
        fp = open("%s/%s"%(base_path, self.docName),"r")
        rawStr = fp.read()
        refinedStr = ''.join([c for c in rawStr if c not in('\n', '\r', ' ')])
        refinedStr = refinedStr.lower()
        # print 'len_content=', len(refinedStr)
        if len(refinedStr)>=50:
            #if refinedStr=="": Yet to be implemented
            thisDoc = FingerprintGenerator(refinedStr)
            thisDoc.generate_fingerprints()
            self.fingerprints = thisDoc.fingerprints
            self.kgrams = thisDoc.kgrams
            self.kLength = thisDoc.kLength
        else:
            self.lessContentFlag = 1

    def get_kgram_from_fp(self):
        '''
        Locates the (consecutive)kgrams for the input doc according the it's match fingerprint
        '''
        for key in self.docToFPDict.keys():
            matchedFP = self.docToFPDict[key]
            tempFPToPos = []
            kgramsPos = []
            for fp in matchedFP:
                for tempList in self.fingerprints:
                    if fp == tempList[0]:
                        tempFPToPos.append([fp,tempList[1]])

            for i in xrange(0,len(tempFPToPos)):
                pos = tempFPToPos[i][1]
                #print '(',self.kgrams[pos], pos,')'
                kgramsPos.append([self.kgrams[pos], pos])
            kgramsPos = sorted(kgramsPos, key=itemgetter(1))
            for item in kgramsPos:
                self.allKgramPos.append(item[1])
            prevString = ""
            tempKlist = []
            flag = 0    # indicates whether at least two previous kgrams are merged or not
            #print '-'*39
            #print kgramsPos
            prevPos = 0
            for i in xrange(0, len(kgramsPos)):
                if i==0:
                    newPos = kgramsPos[0][1]
                    newString = kgramsPos[i][0]
                else:
                    newPos = kgramsPos[i][1]
                    if newPos <= prevPos+self.kLength:
                        temp = self.kgrams[prevPos]
                        truncIndex = -(self.kLength-(newPos-prevPos))
                        if flag == 0:
                            newString = temp[:truncIndex]+self.kgrams[newPos]
                        else:
                            newString = prevString + self.kgrams[newPos][self.kLength-(newPos-prevPos):]	
                        flag = 1
                    else:
                        newString = kgramsPos[i][0]	
                        tempKlist.append(prevString)
                        flag = 0
                prevPos = newPos
                prevString = newString
            tempKlist.append(prevString)
            print tempKlist
            print '-'*40
        t = set(self.allKgramPos)
        self.allKgramPos = list(t)
        self.allKgramPos.sort()
        #print 'allKgramPos=', self.allKgramPos
        self.allInfo.append((self.matchCount/len(self.fingerprints))*100)
        if self.allKgramPos:
            h = highlight_original_text(self.docName, self.allKgramPos, self.kLength)
            #print h
            self.allInfo.append(h)
        #print 'total fingerprints=', len(self.fingerprints)
        #print 'matched fingerprints=', self.matchCount
        #print "percent_similarity=",(self.matchCount/len(self.fingerprints))*100

'''
newDoc = Document("1.txt") 
cProfile.run('newDoc.read_file()')
cProfile.run('newDoc.check()')
cProfile.run('newDoc.sort_match_doc_freq()')
'''
