#!/usr/bin/python

import time
import chilkat
import os
import sys
from httplib import HTTP
from urlparse import urlparse
from celery.decorators import task

from settings import *
from auth.dbmanager.DBHandler import *
from auth.fingerprint.fingerprintGenerator import * 
from auth.crawler.saveHTML import *

@task()
def crawl(address, numberOfPages):
    print 'crawler in action'
    full_address = "http://"+address+"/"
    
    #  The Chilkat Spider component/library is free.
    spider = chilkat.CkSpider()
    spider.put_ProxyLogin("063bct512")
    spider.put_ProxyDomain("10.100.0.1")
    spider.put_ProxyPassword("k")
    spider.put_ProxyPort(8080)

    spider.AddAvoidPattern("*.jsp")
    spider.AddAvoidPattern("*java*")
    spider.AddAvoidPattern("*python*")
    spider.AddAvoidPattern("*perl*")
    spider.AddAvoidPattern("*.css")
    # spider.put_ConnectTimeout(60)

    obj = chilkat.CkHtmlToText()
    #  The spider object crawls a single web site at a time.  As you'll see
    #  in later examples, you can collect outbound links and use them to
    #  crawl the web.  For now, we'll simply spider 10 pages of chilkatsoft.com

    #spider.put_CacheDir("/home/kailash/Desktop/Cache")
    #spider.put_CacheDir("/dev/null")
    #spider.put_FetchFromCache(True)
    #spider.put_UpdateCache(True)
    #  Set our cache directory and make sure saving-to-cache and fetching-from-cache
    #  are both turned on:

    spider.Initialize(address)
    #spider.Initialize("ioe.edu.np")
    #  Add the 1st URL:
    #spider.AddUnspidered("http://ioe.edu.np/")
    spider.AddUnspidered(full_address)
    print address, full_address
    # Do not add URLs longer than 250 characters to the "unspidered" queue:
    spider.put_MaxUrlLen(250)


    # Do not download anything with a response size greater than 100,000 bytes.
    spider.put_MaxResponseSize(300000)

    # Create a folder according to the URL 
    #folder_path = "/home/kailash/workspace/rocop/repo/%s"%address
    folder_path = "%s/%s"%(REPO_PATH, address)
    if not os.path.isdir(folder_path):
        os.mkdir(folder_path)


    #  Avoid URLs matching these patterns:

    #  Begin crawling the site by calling CrawlNext repeatedly.
    j=0
    for i in range(0,numberOfPages):
        success = spider.CrawlNext()
        if (success == True):
            #  Show the URL of the page just spidered.
            url = spider.lastUrl()
            print "URL=", url
            saveDoc(url, j, folder_path)
            j=j+1
            '''
            # The HTML META keywords, title, and description are available in these properties:
            url=spider.lastHtmlDescription()
            print obj.toText(url)
            '''
        else:
            #  Did we get an error or are there no more URLs to crawl?
            if (spider.get_NumUnspidered() == 0):
                print "No more URLs to spider"
            else:
                print spider.lastErrorText()

        #  Sleep 1 second before spidering the next URL.
        if (spider.get_LastFromCache() != True):
            spider.SleepMs(1)

@task()
def saveFP():
    '''
    Saves the fingerprints of the latest webpages i.e., webpages whose fingerprints are not stored in the database
    Maintains a fp.log file to keep track of websites the webpages of whose fingerprints are stored in the database
    '''
    #repo_path = "/home/kailash/workspace/rocop/repo" 
    #f = open("%s/fp.log"%repo_path, "r")
    f = open("%s/fp.log"%REPO_PATH, "r")
    fp_urls_stored = f.read()
    c = "".join([l for l in fp_urls_stored if l not in ("\n")])
    fp_urls_stored = c.split(';')
    f.close()

    #gen_obj = os.walk("%s"%repo_path)
    gen_obj = os.walk("%s"%REPO_PATH)
    repo_dir_tree = gen_obj.next()
    dirs = repo_dir_tree[1] 
    totalTime = 0
    
    for site in dirs: 
        if site not in fp_urls_stored:
            # generate the fingerprint of all .txt files inside the website's directory
            #path = "%s/%s"%(repo_path, site)
            path = "%s/%s"%(REPO_PATH, site)
            txtFilesList = os.walk(path).next()[2]
            for page in txtFilesList:
                idList = page.split('.')
                if page.endswith('.txt'):
                    ff = open("%s/%s"%(path,page), "r")
                    rawString = ff.read()
                    refinedString = ''.join([c for c in rawString if c not in ('\n', '\r', ' ')])
                    refinedString = refinedString.lower()
                    try:
                        newObj = FingerprintGenerator(refinedString)
                        newObj.generate_fingerprints()
                        dbHandler = DBHandler()
                        dbHandler.connect_to_db()
                        docId = "%s-%s"%(site,idList[0])
                        print docId
                        # Get the fingerprint and append it's position with document ID
                        tempTime = time.time()
                        for fprintList in newObj.fingerprints:
                            dbHandler.update_fingerprint_db(fprintList[0], docId)
                        dbHandler.insert_records_into_db()
                        diffTime = time.time()-tempTime
                        totalTime += diffTime
                    except IndexError:
                        print "Technical difficulty: Cannot generate fingerprints for %s"%site 
            #f = open("%s/fp.log"%repo_path, "a")
            f = open("%s/fp.log"%REPO_PATH, "a")
            f.write("%s;"%site)
            f.close()
    print 'totalTime=',totalTime

