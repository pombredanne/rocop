#!/usr/bin/python
#
# templatetags/truncatechars.py - truncates the string to a particular characters
#                                 A custom template tag created for the above purpose
# Copyright (c) 2010, ROCOP
#

#
# Imports
#

from django import template
from django.template.defaultfilters import stringfilter

#
#
#

register = template.Library()

@register.filter(name='truncatechars')
@stringfilter
def truncatechars(value, arg):
    """
    Truncates a string after a certain number of chars.

    Argument: Number of chars to truncate after.
    """
    try:
        length = int(arg)
    except ValueError: # Invalid literal for int().
        return value # Fail silently.
    if len(value) > length:
        return value[:length] + '...'
    return value
truncatechars.is_safe = True
