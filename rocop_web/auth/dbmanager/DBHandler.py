#!/usr/bin/python
#
# dbmanager/DBHandler.py - handles the database related operations
#
# Copyright (c) 2010, ROCOP
#

#
# Imports
#

import MySQLdb 
import sys

#
#
#

#
# The `DBHandler` class
#
class DBHandler:
    '''
    DBHandler objects are used for carrying out operations on DB for individual fingerprints
    '''
    def __init__(self):
        self.conn = ""
        self.cursor = ""
        self.query_prefix = "INSERT INTO `fingerprint` VALUES "
        self.query = ""

    def connect_to_db(self):
        '''
        Establish connection to the MySQL server
        '''
        try: 
            self.conn = MySQLdb.connect(host="localhost",
                                    user="root",
                                    passwd="kailash", 
                                    db="rocop_fp")
            self.cursor = self.conn.cursor()
            #self.cursor.execute("CREATE TABLE IF NOT EXISTS `fingerprint` (fp BIGINT, documents TEXT, INDEX (fp))")
            
        except MySQLdb.Error, e:
            print "Error %d: %s" %(e.args[0], e.args[1])
            print 'insert errror ayoooo'
            sys.exit(1)

    def update_fingerprint_db(self, thisFP, newDocId):
        '''
        Updates the documents field if fingerprint is already present in the database
        '''
        try:
            if self.cursor.execute("UPDATE `fingerprint` SET documents=CONCAT(documents,%s) WHERE fp=%s",(","+newDocId, thisFP))== 0L:
                #self.cursor.execute("INSERT INTO `fingerprint` VALUES (%s, %s)", (thisFP,newDocId)) 		
                self.query = self.query+"(%s, '%s'), "%(thisFP, newDocId)
        except MySQLdb.Error, e:
            print "Error %d: %s" %(e.args[0], e.args[1]) 
            print 'db insertion error'

    def insert_records_into_db(self):
        '''
        Inserts all the records at once at last if fingerprints do not exist in database
        '''
        try:
            if self.query:
                self.query = self.query[:-2]
                temp = self.query_prefix+" "+self.query
                self.cursor.execute(temp)
            self.query = ""
        except MySQLdb.Error, e:
            print self.query
            print 'query error'
        
    def check_fp(self, fprint):
        '''
        Scans the fingerprint table to find the documents for particular fingerprint
        '''
        if self.cursor.execute("SELECT documents from `fingerprint` WHERE fp=%s",fprint) == 0L:
            return 0
        rows = self.cursor.fetchall()
        allDocList = rows[0][0].split(',')
        allDocSet = set(allDocList)
        return list(allDocSet)
        
    def close_db_handler(self):
        '''
        Close the connection to the MySQL server
        '''
        self.cursor.close()
        self.conn.commit()
        self.conn.close()

