#!/usr/bin/python 
#
# Implements multiprocessing to handle multiple client's doc processing in back end
#
# Copyright (c) 2010, ROCOP
#

#
# Imports
#

import multiprocessing, Queue
import os
import sys
from auth.fingerprint.compare import *
from multiprocessing import Pool

#
#
#

#
# Public functions
#

'''
better make a class, use its instance in the views module
so evey client call will use object of that class

def backend_handler(filename):
        print 'filename=', filename
        newDoc = Document(filename)
        newDoc.read_file()
        if newDoc.lessContentFlag:
            return 0
        else:
            newDoc.check()
            newDoc.sort_match_doc_freq()
            return newDoc.allInfo

def handle_backend_processing(timeout, username, filename):
    pool = Pool(processes=1)
    result = pool.apply_async(backend_handler, [filename])
    return result.get(timeout)

'''
def backend_handler(q, filename):
    print 'filename=',filename
    print 'parent process=', os.getppid()
    print 'process id=', os.getpid()
    newDoc = Document(filename) 
    newDoc.read_file()
    if newDoc.lessContentFlag:
        q.put(0)
    else:
        newDoc.check()
        newDoc.sort_match_doc_freq()
        q.put(newDoc.allInfo)

def handle_backend_processing(timeout, username, filename):
    q = multiprocessing.Queue()
    proc = multiprocessing.Process(target=backend_handler, args=(q, filename))
    proc.start()
    try:
        retValue = q.get(timeout)
    except TimeOutError:
        return None
    proc.join(timeout)
    if proc.is_alive():
        proc.terminate()
        return None
    return retValue
'''
# handle_backend_processing(30, 'kailash', '1.txt')

#!/usr/bin/python
# Implements threading to perform the backend processing

import time
import threading
import Queue
import sys
sys.path.append('/home/kailash/workspace/rocop/src/')
from compare import *
result_queue = Queue.Queue()

class testit(threading.Thread):
	def __init__(self, username, filename):
		threading.Thread.__init__(self)
		self.username = username
		self.filename = filename
	
	def run(self):
		global result_queue
		print 'spawning thread for ', self.username
		newDoc = Document(self.filename) 
		newDoc.read_file()
		newDoc.check()
		newDoc.sort_match_doc_freq()
		result_queue.put(newDoc.docToPercentMatch)	
		print 'exiting ... ', self.username
		

def spawnThread(username, filename):
    testit(username, filename).start()
    return result_queue.get()
'''
