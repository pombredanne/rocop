from django.conf.urls.defaults import *
from auth.views import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
 
from django.views.generic.simple import direct_to_template

urlpatterns = patterns('',
    # Example:
    # (r'^rocop_web/', include('rocop_web.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^result/$', result, name="result"), 
    url(r'^update_fp/$', update_fp, name="update_fp"),
    url(r'^report/$', report, name="report"),
    url(r'^moderator/$', moderator, name="moderator"),
    url(r'^accounts/register/$', register, name="register"),
    url(r'^accounts/login/$', login, name="login"),
    url(r'^file_check/$', file_check, name="file_check"),
    url(r'^accounts/logout/$', logout, name="logout"),
    (r'^admin/', include(admin.site.urls)),
    url(r'^$', direct_to_template, {'template':'home.html'}, name='index'), 
    (r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root':'/home/kailash/workspace/rocop/rocop_web/media'})
)
