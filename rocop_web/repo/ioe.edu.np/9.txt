

Home About IOE Alumni Virtual Tour Feedback Search
January 08, 2011


About IOE
Colleges
Autonomous Bodies
Resources
Research
Alumni
Conference/Trainings
Virtual Tour
News/Bulletins
Notice Board
Advanced Search
Feedback
Exam Schedules
Exam Results
CIT CARD CED IECS IRCC CDS CPS CES CIMDU



Institute of Engineering Consultancy Services

Institute of Engineering Consultancy Services (IECS) was established to provide
the consultancy services through the well experienced expertise faculties of
IOE.

Consultancy services can be rendered through Institute of Engineering
Consultancy Services (IECS) and Research Training and Consultancy Unit (RTCU) in
Pulchowk Campus and Consultancy Unit in Thapathali, Eastern Region and Western
Region Campus. IECS generally handles consultancy of multi-disciplinary nature.i

The Clients seeking consultancy services should contact Chief of IECS or Chief
of RTCU in the Pulchowk Campus, the Campus Chief of Thapathali Campus, the
Campus Chief of Eastern Region Campus and the Chief of Consultancy and Training
Service Unit in case of the Western Region Campus.

