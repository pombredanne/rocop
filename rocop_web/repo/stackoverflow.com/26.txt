
$(function() { notify.showFirstTime('Q&A for professional and enthusiast
programmers'); });
Stack Exchange
log in | careers | chat | meta | about | faq

Stack Overflow
    Questions
    Tags
    Users
    Badges
    Unanswered
    Ask Question

marcog

10727
reputation

1129 views

Registered User

name
marcog

member for
1 year, 9 months

seen
3 hours ago

website
marco-za.blogspot.com

location
Cape Town, South Afirca

age
25

MSc student in Computer Science at the University of Cape Town researching
protein-protein interfaces in the field of structural bioinformatics. Been
through 2 x Google internships (working on Code Search and Calendar) and 1 x
NVIDIA internship (working on Direct3d driver debugging).

Follow me on twitter: @marcog

stats activity reputation favorites accounts
13

Questions

votes newest views recent

4
23
votes
4
answers
6
kviews

Python 2.5: JSON module

python json
dec 24 at 20:09 Matt Billenstein 66

3
14
votes
13
answers
771
views

Code Golf: Validate Sudoku Grid

algorithm code-golf sudoku
jan 2 at 14:17 Giuseppe Ottaviano 492

1
10
votes
4
answers
2
kviews

Pipe to/from Clipboard

linux bash clipboard
nov 17 at 19:14 doug 11

4
9
votes
8
answers
2
kviews

Reading Huge File in Python

python performance file-io large-files
mar 14 at 20:25 Jon Seigel 6,273

3
9
votes
12
answers
2
kviews

Python 3 IDE for teaching

python ide python-3.x free education
dec 8 at 17:51 Sridhar Ratnakumar 1,712

2
6
votes
9
answers
2
kviews

Subversion: Retract Accidental Checkin

svn
dec 8 at 12:36 David Fraser 774

3
3
votes
10
answers
659
views

What is the most obfuscated code you&#39;ve had to fix?

debugging obfuscation fun
nov 7 '09 at 16:25 scragar 1,390

2
3
votes
2
answers
433
views

C++ for Python Programmers: Slides and Resources

c++ python resources teaching
apr 25 '09 at 13:49 S.Lott 106k

2
2
votes
3
answers
1
kviews

Advanced LaTeX Tutorial/Book

latex tutorials
apr 22 '09 at 13:54 Jouni K. Seppänen 6,558

1
1
vote
5
answers
165
views

Threading Model

multithreading
apr 28 '09 at 15:46 Groo 5,366

1 2 next
var questionsPageSize = 10; var questionsSortOrder = 'votes';
463

Answers

votes newest views recent

78
Printing 1 to 1000 without loop or conditionals (2)
69
Help with interview question
26
Is Python faster and lighter than C++?
16
In Perl, how can I correctly parse tab/space delimited files with quoted
strings?
14
How to wrap text in LaTeX tables?
14
Haskell lists difference
13
What&#39;s the fastest way in Perl to get all lines of file1 that do not appear
in file2?
13
Work with sets in C++
12
Statement with ? in C
11
Given Prime Number N, Compute the Next Prime?
11
How to Prove one Random Number Generator is Better Than Another?
11
Project Euler 1:Find the sum of all the multiples of 3 or 5 below 1000
11
Maximum number of characters using keystrokes A, Cttrl+A, Ctrl+C and Ctrl+V
10
Determine if a function is available in a Python module
10
Getting the lowest possible sum from numbers&#39; difference
10
Change the key value in python dictionary
10
In Python, can I specify a function argument&#39;s default in terms of other
arguments?
9
In vim, how do I get a file to open at the same line number I closed it at last
time?
9
Move top 1000 lines from text file to a new file using Unix shell commands
9
Regular Expression - is .*$ the same as .*
8
How can I solve The Riddle of Nine 9&#39;s programmatically?
8
Identify empty string in Perl
8
Passing arguments to program run through gdb
8
Compute the length of largest substring that starts and ends with the same
substring
8
Writing to pointer out of bounds after malloc() not causing error
8
Split string at # in C
8
Count letters in a word in python debug
8
Tokenizing a string with unequal number of spaces between fields
8
Moving ahead in python
8
Delete first instance of matching element from array
1 2 3 4 5 &hellip; 16 next
var answersPageSize = 30; var answersSortOrder = 'votes';
1121

Votes

up vote 1079
down vote 42

559

Tags

276
algorithm � 77
254
python � 104
156
java � 67
151
c++ � 43
103
php � 49
97
perl � 25
96
interview-questions � 12
90
language-agnostic � 12
89
arrays � 10
88
regex � 48
80
c � 29
69
matrix
57
string � 20
55
split � 10
44
ruby � 12
44
math � 8
38
mysql � 17
33
performance � 5
32
linux � 12
31
homework � 15
31
memory � 3
26
statistics � 3
25
vim � 8
21
sorting � 8
21
parsing � 5
20
bash � 15
20
shell � 9
18
sql � 6
18
project-euler � 5
18
latex � 4
16
random � 3
16
text-parsing
15
html � 7
14
iterator � 5
14
ubuntu � 3
14
error � 3
14
haskell � 2
14
table
14
wrapping
13
data-structures � 8
13
graph � 7
13
string-manipulation � 7
13
list � 5
13
security � 4
13
debugging � 3
13
set
12
dynamic-programming � 7
12
file � 3
12
count � 2
12
syntax
12
ternary-operator
11
vimrc � 3

1 2 3 4 5 &hellip; 10 next
72

Badges

php
Nice Answer � 18
Talkative
Pundit
Enlightened � 6
Good Answer � 3
Electorate
java
Convention
Strunk & White
c++
Quorum
python
Nice Question � 3
Self-Learner
Peer Pressure
Benefactor
Popular Question � 6
Publicist
Booster
Announcer
Tag Editor
Suffrage
Promoter
algorithm
Disciplined
Civic Duty
Citizen Patrol
Cleanup
Yearling
Notable Question
Mortarboard
Organizer
Commentator
Critic
Autobiographer
Teacher
Scholar
Supporter
Editor
Student

user feed
about | faq | new blog | data | podcast | legal | advertising info | contact us
| feedback always welcome
&#9632; stackoverflow.com &#9632; api/apps &#9632; careers &#9632;
serverfault.com &#9632; superuser.com &#9632; meta &#9632; area 51 &#9632;
webapps &#9632; gaming &#9632; ubuntu &#9632; webmasters &#9632; cooking &#9632;
game development &#9632; math &#9632; photography &#9632; stats &#9632; tex
&#9632; english &#9632; theoretical cs &#9632; programmers
revision: 2011.1.6.1
site design / logo � 2011 stack overflow internet services, inc; user
contributions licensed under cc-wiki with attribution required
Stack Overflow works best with JavaScript enabled
var
_gaq=_gaq||[];_gaq.push(['_setAccount','UA-5620270-1']);_gaq.push(['_trackPagevie
w']);(function(){var
ga=document.createElement('script');ga.type='text/javascript';ga.async=true;ga.sr
c='http://www.google-analytics.com/ga.js';var
s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga,s);})()
;_qoptions={qacct:"p-c1rF4kxgLUzNc"};