
$(function() { notify.showFirstTime('Q&A for professional and enthusiast
programmers'); });
Stack Exchange
log in | careers | chat | meta | about | faq

Stack Overflow
    Questions
    Tags
    Users
    Badges
    Unanswered
    Ask Question

Sean Patrick Floyd

17598
reputation

946 views

Registered User

name
Sean Patrick Floyd

member for
7 months

seen
12 mins ago

website
mostlymagic.wordpress.com

location
Berlin, Germany

age
39

My name is Sean Patrick Floyd, I am a Berlin, Germany based Austrian / American
freelance developer with 15 years of web and 10 years of Java experience. I am
39 years old, and married with three kids. When I don't code or spend time with
my family, I teach Taekwondo.

This is my Blog: mostlymagic.wordpress.com
This is me on Facebook , Xing , Gulp
And my Skype id is seanizer
My user name here also used to be seanizer.

My main coding topics are: Java, Spring, Wicket, Maven (especially with GMaven
), AspectJ.

Favorite IT cartoon: Programming Language Matrix (hilarious)

stats activity reputation favorites accounts
34

Questions

votes newest views recent

13
votes
4
answers
213
views

Is Groovy syntax an exact superset of Java syntax?

java syntax groovy
dec 3 at 20:04 ataylor 3,736

7
votes
2
answers
71
views

What does the Java Compiler do with multiple generic bounds?

java generics type-erasure
nov 23 at 20:55 axtavt 21.3k

1
6
votes
1
answer
180
views

Guava: how to combine filter and transform?

java guava
nov 25 at 13:50 skaffman 64.8k

1
6
votes
6
answers
194
views

In Spring MVC, how can I set the mime type header when using @ResponseBody

java json spring spring-mvc controller
dec 20 at 9:15 Sean Patrick Floyd 17.6k

5
votes
3
answers
124
views

substitution cypher with different alphabet length

java algorithm substitution cypher
aug 30 at 15:07 Sean Patrick Floyd 17.6k

4
votes
3
answers
979
views

Spring AOP pointcut that matches annotation on interface

java spring aop aspectj pointcut
may 23 at 21:52 Espen 2,753

2
4
votes
7
answers
149
views

Find out which classes of a given API are used

java reflection source-code bytecode imports
sep 17 at 18:57 Leo Holanda 110

4
votes
4
answers
223
views

Java time-based map with expiring keys

java caching map guava google-collections
sep 27 at 10:39 Shervin 2,848

4
4
votes
1
answer
86
views

Java Builder generator problem

java code-generation builder-pattern
nov 23 at 17:25 Sean Patrick Floyd 17.6k

1
3
votes
1
answer
163
views

In wicket, combine wicket:link with IAuthorizationStrategy

java authorization wicket
jun 9 at 15:03 Sean Patrick Floyd 17.6k

1 2 3 4 next
var questionsPageSize = 10; var questionsSortOrder = 'votes';
815

Answers

votes newest views recent

48
Accessing the last entry in a Map
16
How to convert an int[] array to a List?
14
Check if a class is subclass of another class in Java
13
Static Method Overloading with Generics
12
What should I call the operation that limits a string&#39;s length?
12
Split string to equal length substrings in Java
12
Converting Array of Primitives to Array of Containers in Java (2)
11
How to define properties for Enum items
11
Java source refactoring of 7000 references
11
In Java, is a char[] an object???
10
In java, What do the return values of Comparable.compareTo mean?
10
Displaying list in Java as elegant as in Python
9
How to write a basic swap function in Java
9
&ldquo;loop:� in Java code. What is this, why does it compile, and generally
WTF?
9
Java: is there a map function?
9
When would it be worth using RegEx in Java?
9
The very basics for using Guava
9
Strange javascript object issue
8
How to round time to the nearest quarter in java?
8
How can i improve this code in Java
8
Guava: Set<K> + Function<K,V> = Map<K,V>?
7
Java question about autoboxing and object equality / identity
7
Can I inject a Java object using Spring without any xml configuration files?
7
Use interface or type for variable definition in java?
7
What are the different ways of performing sorting in java
7
When to use get/set Methods in java
7
Java data structure
6
Rollback on every checked exception, whenever I say @Transactional
6
Converting Java String to ascii
6
Null Pointer Exception and scope in Java
1 2 3 4 5 &hellip; 28 next
var answersPageSize = 30; var answersSortOrder = 'votes';
2333

Votes

up vote 2314
down vote 19

624

Tags

1k
java � 658
205
spring � 143
152
maven-2 � 135
97
hibernate � 56
70
jpa � 40
68
arrays � 16
67
javascript � 53
63
wicket � 38
60
string � 21
44
generics � 20
42
regex � 17
41
guava � 16
40
collections � 16
38
enums � 9
33
maven � 27
31
reflection � 13
29
spring-mvc � 22
27
eclipse � 40
26
html � 22
26
transactions � 15
26
object � 5
25
map � 9
22
split � 6
21
jar � 12
18
orm � 10
18
file � 10
18
class � 5
17
aspectj � 20
17
spring-aop � 11
17
url � 9
17
web-development � 9
17
string-manipulation � 5
17
tools � 3
16
aop � 16
16
unit-testing � 10
16
apache-commons � 8
16
list � 6
16
naming-conventions � 3
16
ide � 3
15
inheritance � 9
15
servlets � 6
14
junit � 12
14
css � 11
14
properties � 9
14
python � 2
13
jpa-2.0 � 8
13
java-ee � 8
13
iterator � 4
13
source � 2
12
annotations � 14
12
oop � 8
12
build-process � 7

1 2 3 4 5 &hellip; 12 next
49

Badges

Enlightened � 3
Nice Answer � 12
Quorum
java
Nice Question
Fanatic
Popular Question
Electorate
spring
Revival
Cleanup
Sportsmanship
Guru
Suffrage
maven-2
java
Strunk & White
Tag Editor
Good Answer
Promoter
Citizen Patrol
Tenacious
Civic Duty
Disciplined
java
Enthusiast
Organizer
Autobiographer
Critic
Mortarboard
Commentator
Supporter
Teacher
Scholar
Student
Editor

user feed
about | faq | new blog | data | podcast | legal | advertising info | contact us
| feedback always welcome
&#9632; stackoverflow.com &#9632; api/apps &#9632; careers &#9632;
serverfault.com &#9632; superuser.com &#9632; meta &#9632; area 51 &#9632;
webapps &#9632; gaming &#9632; ubuntu &#9632; webmasters &#9632; cooking &#9632;
game development &#9632; math &#9632; photography &#9632; stats &#9632; tex
&#9632; english &#9632; theoretical cs &#9632; programmers
revision: 2011.1.6.1
site design / logo � 2011 stack overflow internet services, inc; user
contributions licensed under cc-wiki with attribution required
Stack Overflow works best with JavaScript enabled
var
_gaq=_gaq||[];_gaq.push(['_setAccount','UA-5620270-1']);_gaq.push(['_trackPagevie
w']);(function(){var
ga=document.createElement('script');ga.type='text/javascript';ga.async=true;ga.sr
c='http://www.google-analytics.com/ga.js';var
s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga,s);})()
;_qoptions={qacct:"p-c1rF4kxgLUzNc"};