
$(function() { notify.showFirstTime('Q&A for professional and enthusiast
programmers'); });
Stack Exchange
log in | careers | chat | meta | about | faq

Stack Overflow
    Questions
    Tags
    Users
    Badges
    Unanswered
    Ask Question

Netbeans Shortcut to Open File

up vote 0 down vote favorite
1

I remember seeing someone use a shortcut in NetBeans to open a dialog similar to
phpStrom that can open files based on class names or is it file name. whats
that?

netbeans keyboard-shortcuts screenshot openfiledialog classname
link | flag
edited Jan 1 at 10:28
01
4,470 1 12 37
asked Jan 1 at 7:44
jiewmeng
2,779 1 15

84% accept rate

Some kind of search file feature in NetBeans. � Neigyl Noval Jan 1 at 7:48

Updated my answer, found the plugin. � RobertB Jan 1 at 18:53

3 Answers

active newest votes
up vote 1 down vote accepted

Updated

I'm fairly certain you are referring to the "Quick FileChooser" plugin. As
someone else points out, though, there are several other candidates. I list them
below...

The Quick FileChooser Plugin:

Here's the QFC when opening a project:

... and when opening a file:

I did not find that it was able to open based on class name.

On NetBeans 6.9.1 (and probably earlier versions, maybe not yet for 7.0 beta
though) it is available through the Plugin Portal Update Center, from directly
within NetBeans.

    Click on Tools, then Plugins
    Go to the Settings tab
    Ensure that the "Plugin Portal" is listed in Configuration of Update Centers
    and checked as Active. If it is not listed, click Add, give it an appropriate
    name, and the URL is
    http://plugins.netbeans.org/nbpluginportal/updates/6.9/catalog.xml.gz for
    versions 6.9 and 6.9.1.
    Click on the Available Plugins tab.
    Click on Reload Catalog just to be sure you have the latest cached.
    In Search: type "Quick". That should be enough to get it listed by itself
    (or at least on a short list).
    Click on the checkbox under the Install column, and then click on the
    Install button down below.

By default CTRL

- SHIFT- O opens the Open Project dialog, and once the plugin is installed, you
will get the dialog pictured above automatically.

The Open File dialog does not have a keyboard shortcut by default, but you can
easily add it:

    Click on Tools, then Options, then on the Keymap icon in the tool bar of the
    dialog.
    In Search: type "Open Fi" and you should see "Open File..." in the Actions
    list.
    Double click on the Shortcut box for that entry, and select an appropriate
    shortcut (either by pressing the key combination, or by selecting it from the
    drop-down).
    Click OK.

The Go To... Dialogs:

These are built into the core IDE, no plugin necessary.

The Go To File dialog is ALT

- SHIFT- O.

Go To Type: CTRL

- O, appears to list classes, variables, and all sorts of stuff.

Go To Symbol: CTRL

- ALT- SHIFT- O

For PHP projects, Go To Type and Go To Symbol appear to list the same set. As
mentioned, all of these are available on the Navigate menu.

link | flag
edited Jan 1 at 19:15

answered Jan 1 at 8:14
RobertB
732 7

up vote 0 down vote

you must only hit Ctrl + o keys that show dialog for founding files based on

Class Name ...

link | flag
answered Jan 1 at 8:47
ManWard
45 4

up vote 0 down vote

I use Ctrl-Shift-O to open this dialog for Java classes.
I don't know if this is also valid for PHP though.

If you just want to open some file based on its name, you can use Ctrl-Shift-L

Edit:
Both actions are available in the Navigate menu

link | flag
edited Jan 1 at 9:32

answered Jan 1 at 9:18
a_horse_with_no_name
3,436 2 11

Your Answer

var enableImageUploads = true; $(function() { editorReady('answer', true/*
confirm navigation after wmd keypress */); });
draft saved
log in master.onClickDraftSave('#login-link');
or
Name
Email never shown
Home Page

Not the answer you're looking for? Browse other questions tagged netbeans
keyboard-shortcuts screenshot openfiledialog classname or ask your own question
.

Hello World!

Stack Overflow is a collaboratively edited question and answer site for
programmers � regardless of platform or language. It's 100% free, no
registration required.

about � faq �

tagged

netbeans � 2299
keyboard-shortcuts � 724
screenshot � 338
openfiledialog � 119
classname � 53

asked

4 days ago

viewed

38 times

latest activity

4 days ago

var z = document.createElement("script"); z.type = "text/javascript"; z.async =
true; z.src =
"http://engine2.adzerk.net/z/8277/adzerk1_2_4_43,adzerk2_2_17_45,adzerk3_2_4_44?k
eywords=netbeans,keyboard-shortcuts,screenshot,openfiledialog,classname"; var s
= document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(z, s);
(function(){var
g="http://careers.stackoverflow.com/Ad/js",c=location.search=="?golarge",a=locati
on.hash.indexOf("#ninjas")>-1,b="Pretty";(function(){var
b="min-height:248px;margin:15px
0",c=$("<div>").addClass("hireme").attr("style",b),a=$("<div>").html(c);document.
write(a.html())})();$(function(){a&&$("#adzerk1").html("").append($("<div>").attr
("id","herebeninjas")).attr("id","stop-adzerk");setTimeout(i,1200);setTimeout(h,3
00)});var h=function(){c=c||e();a=a||d();b=c?"Large":b;f()},i=function(){var
a=$("div.hireme");a.each(function(){$(this).html().replace("
","").length==0&&$(this).remove()})},e=function(){return
$("#careersadsdoublehigh").length>0},d=function(){return
$("#herebeninjas").length>0},f=function(){var
a=$("<script>").attr("src",j()).attr("type","text/javascript");$("body").append(a
)},j=function(){return g+"?style="+b+"&gobanner="+a.toString()}})();

Related

Can the .NET OpenFileDialog be setup to allow the user to select a .lnk file
OpenFileDialog always shows *.URL (Internet Shortcut files)
C# OpenFileDialog Stored Paths
Open File Dialog Box
jQuery - replace loop by className
How can I find a Qt metaobject instance from a class name?
What characters are widely supported in CSS class names?
How can I easily get a Scala case class&#39;s name?
How do I get element&#39;s className inside loop of elements?
Delete table in all the rendering pages of a single page in Javascript
getting the name of a class through an instance of that class
Getting name of the class from an instance
C# .cs file name and class name need to be matched?
What does _Default keyword mean in c#
PHP: Get class name of passed var?
Ideal class name for static class for misc functionalities
Is there a common name in use for an object that gets data from a database?
jQuery className woes.
How to convert the current class name of asp.net usercontrols to string on c#?
How to represent class names in LaTeX?
Java - Name for money class
IE6 performance issues with adding className on multiple elements (jQuery
tableHover plugin)
Get Namespace, classname from a dll in C# 2.0
NoClassDefFoundError with a long classname on Tomcat with java 1.4.2_07-b05
Implementing GetByClassName for a .Net XmlDocument.
question feed
default
about | faq | new blog | data | podcast | legal | advertising info | contact us
| feedback always welcome
&#9632; stackoverflow.com &#9632; api/apps &#9632; careers &#9632;
serverfault.com &#9632; superuser.com &#9632; meta &#9632; area 51 &#9632;
webapps &#9632; gaming &#9632; ubuntu &#9632; webmasters &#9632; cooking &#9632;
game development &#9632; math &#9632; photography &#9632; stats &#9632; tex
&#9632; english &#9632; theoretical cs &#9632; programmers
revision: 2011.1.6.1
site design / logo � 2011 stack overflow internet services, inc; user
contributions licensed under cc-wiki with attribution required
Stack Overflow works best with JavaScript enabled
var
_gaq=_gaq||[];_gaq.push(['_setAccount','UA-5620270-1']);_gaq.push(['_trackPagevie
w']);(function(){var
ga=document.createElement('script');ga.type='text/javascript';ga.async=true;ga.sr
c='http://www.google-analytics.com/ga.js';var
s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga,s);})()
;_qoptions={qacct:"p-c1rF4kxgLUzNc"};