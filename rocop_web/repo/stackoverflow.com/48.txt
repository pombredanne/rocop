$(function() { notify.showFirstTime('Q&A for professional and enthusiast
programmers'); }); Stack Exchange log in | careers | chat | meta | about | faq
Stack Overflow Questions Tags Users Badges Unanswered Ask Question Threading and
pcap issues. up vote 0 down vote favorite I have a GUI program that allows a
user a scan a network, the issue is that when the pcap_loop function is called,
my GUI program becomes unresponsive.(the pcap_loop blocks the current thread).
When i try to use pthreads, i got a SIGSEGV fault at the pcap_loop
function.Why?It's as if the thread can't see the procPacket function itself.
void procPacket(u_char *arg, const struct pcap_pkthdr *pkthdr, const u_char
*packet){ //show packets here}void* pcapLooper(void* param){ pcap_t* handler =
(pcap_t*) param; pcap_loop(handler, 900 ,procPacket, NULL );} //some function
that runs when a button is pressed //handler has been opened through
pcap_open_live pthread_t scanner; int t =
pthread_create(&scanner,NULL,&pcapLooper, &handler ); if(t) {
std::cout << "failed" << std::endl; } pthread_join(scanner,NULL);
//do other stuff.
c++ pcap libpcap
link | flag
asked Jan 1 at 9:35
cftmon
1

2 Answers

active newest votes
up vote 1 down vote

I would strongly suggest not using threads unless you absolutely have to.

The problem is that you have to be extremely careful to avoid race conditions
and other synchronisation problems. For instance, your GUI framework library is
probably not expecting to be called from multiple threads, and so your //show
packets here routine may confuse it a lot.

Instead I would suggest, if possible, reading the packets from the main thread.
You don't say which GUI framework you're using; since you're using C++ I'll
assume Qt since that's quite common, but all other frameworks have similar
functionality.

What you need to do is:

    Call pcap_setnonblock() to put the capture descriptor in non-blocking mode
    Call pcap_get_selectable_fd() to get a file descriptor to monitor for events
    Use a QSocketNotifier object (passing the file descriptor from the previous
    step as the socket parameter) to monitor the file descriptor for events
    When the event fires, call pcap_dispatch() to dispatch packets
    For maximum portability, also call pcap_dispatch() on a timer, because
    select() doesn't work well on pcap sockets on some OSs.

(As to why your code is currently crashing - note that you probably want to pass
handler rather than &handler as the parameter to pthread_create() . But just
fixing that may lead to strange unreliability later - so going single-threaded
is almost certainly the way forward!)

link | flag
answered Jan 1 at 11:46
psmears
4,086 3 8

up vote 0 down vote

You need much less "&"s. Assuming

pcap_t *handle = pcap_open_live(...);

using &handle will be of type pcap_t ** , but your thread function casts it
back (the cast by the way is also pointless/redundant) to pcap_t * , which leads
to undefined behavior when using it, and is usually going to go wrong. Better:

static void *pcap_looper(void *arg){        pcap_t *handle = arg;        /* etc. */        return NULL;}int main(void){        pcap_t *handle;        pthread_t tid;        int ret;        handle = pcap_open_live(...);        ret = pthread_create(&tid, NULL, pcap_looper, handle);        ...        pthread_join(tid, NULL);        return EXIT_SUCCESS;}
link | flag
answered Jan 1 at 11:42
user502515
1,561 10

Your Answer

var enableImageUploads = true; $(function() { editorReady('answer', true/*
confirm navigation after wmd keypress */); });
draft saved
log in master.onClickDraftSave('#login-link');
or
Name
Email never shown
Home Page

Not the answer you're looking for? Browse other questions tagged c++ pcap
libpcap or ask your own question .

Hello World!

Stack Overflow is a collaboratively edited question and answer site for
programmers � regardless of platform or language. It's 100% free, no
registration required.

about � faq �

tagged

c++ � 53130
pcap � 83
libpcap � 56

asked

4 days ago

viewed

48 times

latest activity

4 days ago

var z = document.createElement("script"); z.type = "text/javascript"; z.async =
true; z.src =
"http://engine2.adzerk.net/z/8277/adzerk1_2_4_43,adzerk2_2_17_45,adzerk3_2_4_44?k
eywords=c%2b%2b,pcap,libpcap"; var s =
document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(z, s);
(function(){var
g="http://careers.stackoverflow.com/Ad/js",c=location.search=="?golarge",a=locati
on.hash.indexOf("#ninjas")>-1,b="Pretty";(function(){var
b="min-height:248px;margin:15px
0",c=$("<div>").addClass("hireme").attr("style",b),a=$("<div>").html(c);document.
write(a.html())})();$(function(){a&&$("#adzerk1").html("").append($("<div>").attr
("id","herebeninjas")).attr("id","stop-adzerk");setTimeout(i,1200);setTimeout(h,3
00)});var h=function(){c=c||e();a=a||d();b=c?"Large":b;f()},i=function(){var
a=$("div.hireme");a.each(function(){$(this).html().replace("
","").length==0&&$(this).remove()})},e=function(){return
$("#careersadsdoublehigh").length>0},d=function(){return
$("#herebeninjas").length>0},f=function(){var
a=$("<script>").attr("src",j()).attr("type","text/javascript");$("body").append(a
)},j=function(){return g+"?style="+b+"&gobanner="+a.toString()}})();

Related

PCap performance
Why ruby&#39;s(ver 1.9) pcap gem, hanging after accessing it?
Ruby/C/Makefile, what is the default pcap.h file that is used in -lpcap/#include
<pcap.h>
removing payload from the captured pcap files
pcap only picking up on new connections
Packet Logging with Pcap for C#
zero read timeout for pcap
Pcap library for C# odd problem
C pcap detecting inbound datagrams
pcap function: pcap_open() question&hellip;
How to filter POST requests using pcap?
collect packet length in pcap file
Reconstructing data from PCAP sniff
How restore data from pcap file?
trackTCP Streams among a pcap file
Detecting video bitrates from a PCAP dump of a (progressive download) stream
pcap datalink LINUX_SLL
Determining type of pcap file.
How to send pcap file packets on NIC?
pcap struct pcap_pkthdr len vs caplen
Using pcap in VS .NET 2003 and error C2085, C2061
question about pcap
help using pcap library to sniff packets
Capturing network traffic in ruby - pcap related issues
missing elements from pcap?
question feed
lang-c
about | faq | new blog | data | podcast | legal | advertising info | contact us
| feedback always welcome
&#9632; stackoverflow.com &#9632; api/apps &#9632; careers &#9632;
serverfault.com &#9632; superuser.com &#9632; meta &#9632; area 51 &#9632;
webapps &#9632; gaming &#9632; ubuntu &#9632; webmasters &#9632; cooking &#9632;
game development &#9632; math &#9632; photography &#9632; stats &#9632; tex
&#9632; english &#9632; theoretical cs &#9632; programmers
revision: 2011.1.6.1
site design / logo � 2011 stack overflow internet services, inc; user
contributions licensed under cc-wiki with attribution required
Stack Overflow works best with JavaScript enabled
var
_gaq=_gaq||[];_gaq.push(['_setAccount','UA-5620270-1']);_gaq.push(['_trackPagevie
w']);(function(){var
ga=document.createElement('script');ga.type='text/javascript';ga.async=true;ga.sr
c='http://www.google-analytics.com/ga.js';var
s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga,s);})()
;_qoptions={qacct:"p-c1rF4kxgLUzNc"};